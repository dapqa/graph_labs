package graph.labs.lab5

import graph.labs.common.Graph
import graph.labs.common.findDirectTransitiveClosure
import graph.labs.common.makeTransposedGraph
import graph.labs.common.nodeNumbersToNames2
import java.util.*

/**
 * @author Nechaev Alexander
 * Поиск сильных компонент методом Мальгранжа (последовательный)
 */

// Поиск одного сильного компонента через прямое и обратное транзитивные замыкания
private fun findOneStrongComponentMalgrange(graph: Graph, from: Int): List<Int> {
    val dirTransitiveClosure = findDirectTransitiveClosure(graph, from)
    val invTransitiveClosure = findDirectTransitiveClosure(makeTransposedGraph(graph), from)

    val intersection = ArrayList<Int>()
    dirTransitiveClosure.forEach {
        if (invTransitiveClosure.contains(it)) {
            intersection.add(it)
        }
    }

    return intersection
}

// Последовательное разбиение графа на подграфы через findOneStrongComponentMalgrange
fun findStrongComponentsMalgrange(graph: Graph): List<List<Int>> {
    val res = ArrayList<List<Int>>()
    val nodeCount = graph.nodeCount

    val visited = Array(nodeCount, { false })
    var visitedCount = 0

    val adjMatrixCopy = Array(nodeCount, { graph.adjacencyMatrix[it].clone() })
    val temp = Graph()
    temp.adjacencyMatrix = adjMatrixCopy

    var from = START_FROM
    var newVisited: Boolean
    while (visitedCount < nodeCount) {
        val component = findOneStrongComponentMalgrange(temp, from)
        res.add(component)

        newVisited = false
        component.forEach {
            for (i in 0..nodeCount - 1) {
                temp.adjacencyMatrix[it][i] = 0
                temp.adjacencyMatrix[i][it] = 0
            }

            if (!visited[it]) {
                visitedCount++
                visited[it] = true
                newVisited = true
            }
        }

        if (!newVisited) {
            visitedCount++
            visited[from] = true
        }

        if (visitedCount < nodeCount) {
            for (i in 0..nodeCount - 1) {
                if (!visited[i]) {
                    from = i
                    break
                }
            }
        }
    }

    // Поправка результата - вычищение пустых графов
    val fixedRes = ArrayList<List<Int>>()

    for (i in 0..nodeCount - 1) {
        visited[i] = false
    }

    res.forEach {
        if (it.size > 0) {
            fixedRes.add(it)
            it.forEach { visited[it] = true }
        }
    }

    for (i in 0..nodeCount - 1) {
        if (!visited[i]) {
            fixedRes.add(Collections.singletonList(i))
        }
    }

    return fixedRes
}

fun findStrongComponentsMalgrangeVNames(graph: Graph): List<List<String>> {
    return nodeNumbersToNames2(graph, findStrongComponentsMalgrange(graph))
}