package graph.labs.lab5.controller

import graph.labs.common.Graph
import graph.labs.common.controller.AdjacencyMatrixController
import graph.labs.common.controller.BaseLabController
import graph.labs.common.controller.GraphPaneController
import graph.labs.common.controller.ParallelCheckerPaneController
import graph.labs.common.findIntersection
import graph.labs.lab5.findStrongComponentsMalgrange
import graph.labs.lab5.findStrongComponentsMalgrangeParallel
import graph.labs.lab5.findStrongComponentsMalgrangeVNames
import javafx.fxml.FXML
import javafx.scene.control.Label
import java.util.function.Consumer

/**
 * @author Nechaev Alexander
 */
class Lab5Controller : BaseLabController() {

    @FXML lateinit var graph1PaneController: GraphPaneController
    @FXML lateinit var graph2PaneController: GraphPaneController
    @FXML lateinit var graph3PaneController: GraphPaneController
    @FXML lateinit var adjacencyMatrix1Controller: AdjacencyMatrixController
    @FXML lateinit var adjacencyMatrix2Controller: AdjacencyMatrixController
    @FXML lateinit var adjacencyMatrix3Controller: AdjacencyMatrixController
    @FXML lateinit var parallelCheckerPaneController: ParallelCheckerPaneController
    @FXML lateinit var strongComponentsLabel: Label

    private val MATRIX1_ID = "M1"
    private val MATRIX2_ID = "M2"

    var graph1: Graph = Graph()
    var graph2: Graph = Graph()
    var graph3: Graph = Graph()

    var regeneratedOnce = false

    @FXML
    fun initialize() {
        initAdjacencyMatrix(adjacencyMatrix1Controller, MATRIX1_ID)
        initAdjacencyMatrix(adjacencyMatrix2Controller, MATRIX2_ID)

        adjacencyMatrix3Controller.disableGenerating()

        parallelCheckerPaneController.maxNodeCount = 6400
        parallelCheckerPaneController.seqConsumer = Consumer { g -> findStrongComponentsMalgrange(g) }
        parallelCheckerPaneController.parConsumer = Consumer { g -> findStrongComponentsMalgrangeParallel(g) }
    }

    override fun onRegenerate(id: String) {
        super.onRegenerate(id)

        if (!regeneratedOnce) {
            regeneratedOnce = true
            return
        }

        graph3 = findIntersection(graph1, graph2)
        adjacencyMatrix3Controller.refreshMatrix(graph3.adjacencyMatrix)
        graph3PaneController.drawGraph(graph3)

        strongComponentsLabel.text = findStrongComponentsMalgrangeVNames(graph3).toString()
    }

    override fun getGUIGraphDescriptorById(id: String): BaseLabController.GUIGraphDescriptor {
        if (MATRIX1_ID == id) {
            return BaseLabController.GUIGraphDescriptor(graph1, adjacencyMatrix1Controller, graph1PaneController)
        } else {
            return BaseLabController.GUIGraphDescriptor(graph2, adjacencyMatrix2Controller, graph2PaneController)
        }
    }

    override fun getDefaultNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMinNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMaxNodeCountToGenerate(): Int {
        return 10
    }

}