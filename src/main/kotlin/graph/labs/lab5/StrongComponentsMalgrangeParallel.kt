package graph.labs.lab5

import graph.labs.common.CORE_COUNT
import graph.labs.common.Graph
import graph.labs.common.findDirectTransitiveClosure
import graph.labs.common.makeTransposedGraph
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * @author Nechaev Alexander
 * Поиск сильных компонент методом Мальгранжа (параллельный)
 */

// Поиск одного сильного компонента через прямое и обратное транзитивные замыкания, которые расчитываются
// в отдельных потоках
private fun findOneStrongComponentMalgrangeParallel(graph: Graph, from: Int): List<Int> {
    val dirTransitiveClosure = ArrayList<Int>()
    val invTransitiveClosure = ArrayList<Int>()

    var executorService = Executors.newFixedThreadPool(CORE_COUNT)
    executorService.execute {
        dirTransitiveClosure.addAll(findDirectTransitiveClosure(graph, from))
    }
    executorService.execute {
        invTransitiveClosure.addAll(findDirectTransitiveClosure(makeTransposedGraph(graph), from))
    }
    executorService.shutdown()
    executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)

    val intersection = ArrayList<Int>()
    dirTransitiveClosure.forEach {
        if (invTransitiveClosure.contains(it)) {
            intersection.add(it)
        }
    }

    return intersection
}

// Последовательное разбиение графа на подграфы через findOneStrongComponentMalgrangeParallel
fun findStrongComponentsMalgrangeParallel(graph: Graph): List<List<Int>> {
    val res = ArrayList<List<Int>>()
    val nodeCount = graph.nodeCount

    val visited = Array(nodeCount, { false })
    var visitedCount = 0

    val adjMatrixCopy = Array(nodeCount, { graph.adjacencyMatrix[it].clone() })
    val temp = Graph()
    temp.adjacencyMatrix = adjMatrixCopy

    var from = START_FROM
    var newVisited: Boolean
    while (visitedCount < nodeCount) {
        val component = findOneStrongComponentMalgrangeParallel(temp, from)
        res.add(component)

        newVisited = false
        component.forEach {
            for (i in 0..nodeCount - 1) {
                temp.adjacencyMatrix[it][i] = 0
                temp.adjacencyMatrix[i][it] = 0
            }

            if (!visited[it]) {
                visitedCount++
                visited[it] = true
                newVisited = true
            }
        }

        if (!newVisited) {
            visitedCount++
            visited[from] = true
        }

        if (visitedCount < nodeCount) {
            for (i in 0..nodeCount - 1) {
                if (!visited[i]) {
                    from = i
                    break
                }
            }
        }
    }

    // Поправка результата - вычищение пустых графов
    val fixedRes = ArrayList<List<Int>>()

    for (i in 0..nodeCount - 1) {
        visited[i] = false
    }

    res.forEach {
        if (it.size > 0) {
            fixedRes.add(it)
            it.forEach { visited[it] = true }
        }
    }

    for (i in 0..nodeCount - 1) {
        if (!visited[i]) {
            fixedRes.add(Collections.singletonList(i))
        }
    }

    return fixedRes
}
