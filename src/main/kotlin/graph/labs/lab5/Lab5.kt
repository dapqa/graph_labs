package graph.labs.lab5

import graph.labs.common.BaseLab
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */
class Lab5 : BaseLab<Lab5>() {

    override fun getLayout(): String {
        return "Lab5.fxml"
    }

    override fun getLabClass(): KClass<Lab5> {
        return Lab5::class
    }

    override fun getName(): String {
        return "Лабораторная работа #5 по ТиМДВ"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Lab5::class.java)
        }
    }

}

val START_FROM = 4;