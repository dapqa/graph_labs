package graph.labs.lab1.controller

import graph.labs.common.Graph
import graph.labs.common.controller.AdjacencyMatrixController
import graph.labs.common.controller.BaseLabController
import graph.labs.common.controller.GraphPaneController
import graph.labs.common.controller.showError
import javafx.event.Event
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label

/**
 * @author Nechaev Alexander
 */

class Lab1Controller : BaseLabController() {

    @FXML lateinit var graph1PaneController: GraphPaneController
    @FXML lateinit var graph2PaneController: GraphPaneController
    @FXML lateinit var adjacencyMatrix1Controller: AdjacencyMatrixController
    @FXML lateinit var adjacencyMatrix2Controller: AdjacencyMatrixController
    @FXML lateinit var identifyBtn: Button
    @FXML lateinit var node1Box: ChoiceBox<String>
    @FXML lateinit var node2Box: ChoiceBox<String>
    @FXML lateinit var maxIndegreeNodesLabel: Label

    private val MATRIX1_ID = "M1"
    private val MATRIX2_ID = "M2"

    var graph1: Graph = Graph()
    var graph2: Graph = Graph()

    @FXML
    fun initialize() {
        initAdjacencyMatrix(adjacencyMatrix1Controller, MATRIX1_ID)
        initAdjacencyMatrix(adjacencyMatrix2Controller, MATRIX2_ID)
    }

    override fun getGUIGraphDescriptorById(id: String): GUIGraphDescriptor {
        return if (id == MATRIX1_ID)
            GUIGraphDescriptor(graph1, adjacencyMatrix1Controller, graph1PaneController) else
            GUIGraphDescriptor(graph2, adjacencyMatrix2Controller, graph2PaneController)
    }

    override fun getDefaultNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMinNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMaxNodeCountToGenerate(): Int {
        return 10
    }

    fun identify(event: Event?) {
        if (node1Box.selectionModel.selectedIndex == node2Box.selectionModel.selectedIndex) {
            showError("Выберите разные вершины")
            return
        }

        graph1.identify(node1Box.selectionModel.selectedIndex, node2Box.selectionModel.selectedIndex)
        graph1PaneController.drawGraph(graph1)
        adjacencyMatrix1Controller.refreshMatrix(graph1.adjacencyMatrix, graph1.nodeNames)

        node1Box.items.clear()
        node2Box.items.clear()
        node1Box.isDisable = true
        node2Box.isDisable = true
        identifyBtn.isDisable = true
    }

    override fun onRegenerate(id: String) {
        super.onRegenerate(id)

        if (id == MATRIX1_ID) {
            node1Box.items.clear()
            node2Box.items.clear()
            graph1.nodeNames.forEach {
                node1Box.items.add(it)
                node2Box.items.add(it)
            }
            node1Box.selectionModel.selectFirst()
            node2Box.selectionModel.selectFirst()

            node1Box.isDisable = false
            node2Box.isDisable = false
            identifyBtn.isDisable = false
        } else {
            maxIndegreeNodesLabel.text = graph2.getMaxIndegreeNodeNames().toString()
        }
    }

}