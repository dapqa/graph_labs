package graph.labs.lab1

import graph.labs.common.BaseLab
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */
class Lab1 : BaseLab<Lab1>() {

    override fun getLayout(): String {
        return "Lab1.fxml"
    }

    override fun getLabClass(): KClass<Lab1> {
        return Lab1::class
    }

    override fun getName(): String {
        return "Лабораторная работа #1 по ТиМДВ"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Lab1::class.java)
        }
    }

}