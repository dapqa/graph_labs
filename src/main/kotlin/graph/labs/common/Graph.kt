package graph.labs.common

import java.util.*

/**
 * @author Nechaev Alexander
 * Пакет содержит класс графа и некоторые вспомогательные методы
 */

class Graph(var oriented: Boolean = true, var weighted: Boolean = false) {

    private val random = Random()

    var maxNodeCount: Int = 100000
    var minNodeCount: Int = 2

    // Настройки случайной генерации
    // Как правило, взвешенные графы для задач нужны более плотные, поэтому для них отдельная настройка
    private val EDGE_GENERATION_FACTOR_MAX: Int = 10
    private val EDGE_GENERATION_FACTOR_VALUE: Int = 3 // чем выше - тем больше ребер (для невзвешенного)
    private val WEIGHTED_EDGE_GENERATION_FACTOR_VALUE: Int = 8 // чем выше - тем больше ребер (для взвешенного)
    private val MAX_GENERATED_WEIGHT: Int = 20

    var nodeCount: Int = 5
        private set(value) {
            if (value !in minNodeCount..maxNodeCount) {
                throw IllegalArgumentException("Количество вершин должно быть в промежутке от $minNodeCount до $maxNodeCount")
            }

            nodeNames.clear()
            (1..value).forEach { nodeNames.add("$it") }

            field = value
        }

    var adjacencyMatrix: Array<ByteArray> = Array(nodeCount, { i -> ByteArray(nodeCount) })
        set(value) {
            if (value.size == 0 || value.size != value[0].size) {
                throw IllegalArgumentException("Матрица смежности должна быть квадратной размером от $minNodeCount до $maxNodeCount")
            }
            nodeCount = value.size

            field = value
        }

    // Имена вершин хранятся отдельно, т.к. могут отличаться от номеров
    val nodeNames = ArrayList<String>()

    // Генерирует граф заново с указанным количеством вершин
    fun regenerate(nodeCount: Int) {
        this.nodeCount = nodeCount
        adjacencyMatrix = Array(nodeCount, { ByteArray(nodeCount) })

        val factor = if (weighted) WEIGHTED_EDGE_GENERATION_FACTOR_VALUE else EDGE_GENERATION_FACTOR_VALUE;
        if (oriented) {
            for (i in 0..nodeCount - 1) {
                for (j in 0..nodeCount - 1) {
                    if (random.nextInt(EDGE_GENERATION_FACTOR_MAX) <= factor) {
                        adjacencyMatrix[i][j] = 1
                        if (weighted) {
                            adjacencyMatrix[i][j] = (adjacencyMatrix[i][j] * (random.nextInt(MAX_GENERATED_WEIGHT) + 1)).toByte()
                        }
                    }
                }
            }
        } else {
            // TODO Здесь ведь только половину матрицы надо генерить
            for (i in 0..nodeCount - 1) {
                for (j in 0..nodeCount - 1) {
                    if (random.nextInt(EDGE_GENERATION_FACTOR_MAX) <= factor) {
                        adjacencyMatrix[i][j] = 1
                        if (weighted) {
                            adjacencyMatrix[i][j] = (adjacencyMatrix[i][j] * (random.nextInt(MAX_GENERATED_WEIGHT) + 1)).toByte()
                        }

                        adjacencyMatrix[j][i] = adjacencyMatrix[i][j]
                    }
                }
            }
        }
    }

    // Отождествление вершин
    fun identify(n0: Int, n1: Int) {
        val minN = Math.min(n0, n1)
        val maxN = Math.max(n0, n1)
        val newAdjMatrix = Array(nodeCount - 1, { ByteArray(nodeCount - 1) })
        for (j in 0..nodeCount - 1) {
            adjacencyMatrix[minN][j] = (adjacencyMatrix[minN][j].toInt() or adjacencyMatrix[maxN][j].toInt()).toByte()
        }
        for (i in 0..nodeCount - 1) {
            adjacencyMatrix[i][minN] = (adjacencyMatrix[i][minN].toInt() or adjacencyMatrix[i][minN].toInt()).toByte()
        }

        var resI = 0
        var resJ: Int
        for (i in 0..nodeCount - 1) {
            if (i == maxN) continue
            resJ = 0
            for (j in 0..nodeCount - 1) {
                if (j == maxN) continue
                newAdjMatrix[resI][resJ] = adjacencyMatrix[i][j]
                resJ += 1
            }
            resI += 1
        }

        adjacencyMatrix = newAdjMatrix

        // Замена имени вершины, которая является отождествлением А и Б, на А-Б
        nodeNames.set(minN, "${minN + 1}-${maxN + 1}")
        (minN + 1..nodeCount - 1).forEach {
            nodeNames.set(it, "${if (it >= maxN) it + 2 else it + 1}")
        }
    }

    // Возвращает имена вершин с максимальной степенью полузахода
    fun getMaxIndegreeNodeNames(): ArrayList<String> {
        val res = ArrayList<String>()

        var max = 0
        for (j in 0..nodeCount - 1) {
            var curIndegree = 0;
            for (i in 0..nodeCount - 1) {
                curIndegree += adjacencyMatrix[i][j]
            }
            if (curIndegree == max) {
                res.add(nodeNames.get(j))
            } else if (curIndegree > max) {
                max = curIndegree
                res.clear()
                res.add(nodeNames.get(j))
            }
        }

        return res
    }

    // Создает матрицу инциденций, соответсвующую хранящейся внутри матрице смежности
    fun makeIncidenceMatrix(): IncidenceMatrix {
        var edgeCount = 0
        val nodeCount = adjacencyMatrix.size
        for (i in 0..nodeCount - 1) {
            for (j in 0..nodeCount - 1) {
                if (adjacencyMatrix[i][j].toInt() == 1) edgeCount++
            }
        }

        val resMatrix = Array(nodeCount, { ByteArray(edgeCount, { 0 }) })
        val resEdgeNames = ArrayList<String>()
        var curEdge = 0
        for (i in 0..nodeCount - 1) {
            for (j in 0..nodeCount - 1) {
                if (adjacencyMatrix[i][j].toInt() == 1) {
                    if (i != j) {
                        resMatrix[j][curEdge] = -1
                        resMatrix[i][curEdge] = 1
                    } else {
                        resMatrix[i][curEdge] = 2
                    }

                    resEdgeNames.add("${nodeNames.get(i)}-${nodeNames.get(j)}")

                    curEdge++
                }
            }
        }

        return IncidenceMatrix(resMatrix, nodeNames, resEdgeNames)
    }

}

data class IncidenceMatrix(val matrix: Array<ByteArray>, val nodeNames: List<String>, val edgeNames: List<String>)

// Два вспомогательных метода для перевода списков с номерами вершин в списки с их именами
fun nodeNumbersToNames(graph: Graph, nums: List<Int>): List<String> {
    val res = ArrayList<String>()
    nums.forEach { res.add(graph.nodeNames.get(it)) }
    return res
}

fun nodeNumbersToNames2(graph: Graph, nums: List<List<Int>>): List<List<String>> {
    val res = ArrayList<List<String>>()
    nums.forEach { res.add(nodeNumbersToNames(graph, it)) }
    return res
}

// Метод для транспонирования матрицы
fun transpose(matrix: Array<ByteArray>): Array<ByteArray> {
    val height = matrix.size
    val width = matrix[0].size
    val res = Array(height, { ByteArray(width, { 0 }) })
    for (i in 0..height - 1) {
        for (j in 0..width - 1) {
            res[i][j] = matrix[j][i]
        }
    }
    return res
}

// Метод для транспонирования графа
fun makeTransposedGraph(graph: Graph): Graph {
    val adjMatrix = graph.adjacencyMatrix
    val resAdjMatrix = transpose(adjMatrix)

    val res = Graph()
    res.adjacencyMatrix = resAdjMatrix
    return res
}

// Метод, который находит прямое транзитивное замыкание для вершины nodeNumber
fun findDirectTransitiveClosure(graph: Graph, nodeNumber: Int): List<Int> {
    val nodeCount = graph.nodeCount
    val adjMatrix = graph.adjacencyMatrix

    var visitedCount = 0
    val visited = Array(nodeCount, { 0 })
    val res = ArrayList<Int>()

    visited[nodeNumber] = 1
    var prevVisitedCount: Int
    do {
        prevVisitedCount = visitedCount

        for (i in 0..nodeCount - 1) {
            if (visited[i] == 1) {
                for (j in 0..nodeCount - 1) {
                    if (adjMatrix[i][j].toInt() == 1 && (visited[j] == 0 || j == nodeNumber && visited[j] == 1)) {
                        visited[j]++
                        visitedCount++
                    }
                }
            }
        }
    } while (prevVisitedCount != visitedCount)
    visited[nodeNumber]--

    for (i in 0..nodeCount - 1) {
        if (visited[i] == 1) {
            res.add(i)
        }
    }

    return res
}

// Возвращает граф, который является пересечением двух других
fun findIntersection(graph1: Graph, graph2: Graph): Graph {
    val nodeCount = Math.min(graph1.nodeCount, graph2.nodeCount)
    val resAdjMatrix = Array(nodeCount, { ByteArray(nodeCount, { 0 }) })

    for (i in 0..nodeCount - 1) {
        for (j in 0..nodeCount - 1) {
            resAdjMatrix[i][j] = (graph1.adjacencyMatrix[i][j].toInt() and graph2.adjacencyMatrix[i][j].toInt()).toByte()
        }
    }

    val res = Graph()
    res.adjacencyMatrix = resAdjMatrix
    return res
}

// Класс и метод ниже - для генерации списка ребер для графа
data class Edge(val from: Int, val to: Int, val weight: Int)

fun makeEdgeList(graph: Graph): List<Edge> {
    val adjMatrix = graph.adjacencyMatrix
    val nodeCount = graph.nodeCount

    val res = ArrayList<Edge>()
    for (i in 0..nodeCount - 1) {
        for (j in 0..nodeCount - 1) {
            if (adjMatrix[i][j] != 0.toByte()) {
                res.add(Edge(i, j, adjMatrix[i][j].toInt()))
            }
        }
    }

    return res
}