package graph.labs.common

import javafx.application.Application
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */

abstract class BaseLab<T: Any> : Application() {

    override fun start(primaryStage: Stage?) {
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

        primaryStage?.scene = Scene(FXMLLoader.load<Parent?>(getLabClass().java.getResource(getLayout())))
        primaryStage?.title = "${getName()}. Выполнил Нечаев Александр Александрович, ИВТм-11-01, 2016"
        primaryStage?.isResizable = false
        primaryStage?.sizeToScene()
        primaryStage?.onCloseRequest = EventHandler {
            Platform.exit()
            System.exit(0)
        }

        primaryStage?.show()
    }

    protected abstract fun getLayout(): String

    protected abstract fun getLabClass(): KClass<T>

    protected abstract fun getName(): String

}