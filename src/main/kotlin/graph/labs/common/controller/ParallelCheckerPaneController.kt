package graph.labs.common.controller

import com.sun.javafx.collections.ObservableListWrapper
import graph.labs.common.Graph
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import java.util.function.Consumer

/**
 * @author Nechaev Alexander
 */

class ParallelCheckerPaneController {

    @FXML lateinit var graphSizeChoiceBox: ChoiceBox<Int>
    @FXML lateinit var seqTimeLabel: Label
    @FXML lateinit var parTimeLabel: Label
    @FXML lateinit var startBtn: Button

    var seqConsumer: Consumer<Graph>? = null
    var parConsumer: Consumer<Graph>? = null
    var useWeighted = false
    var maxNodeCount = 2048
        set(value) {
            graphSizeChoiceBox.items = ObservableListWrapper((256..value step 256).plus(64..128 step 64).sorted())
            graphSizeChoiceBox.selectionModel.selectFirst()
            field = value
        }

    @FXML
    fun initialize() {
        // Это вызовет сеттер и проинициализирует
        maxNodeCount = maxNodeCount

        startBtn.onAction = EventHandler { e ->
            if (seqConsumer == null || parConsumer == null) return@EventHandler

            val calcThread = Thread({
                Platform.runLater({
                    graphSizeChoiceBox.isDisable = true
                    startBtn.isDisable = true
                })

                val nodeCount = graphSizeChoiceBox.selectionModel.selectedItem

                val graph = Graph(weighted = useWeighted)
                graph.regenerate(nodeCount)

                var startTime = System.currentTimeMillis()
                seqConsumer?.accept(graph)
                var endTime = System.currentTimeMillis()
                val seqTime = endTime - startTime

                startTime = System.currentTimeMillis()
                parConsumer?.accept(graph)
                endTime = System.currentTimeMillis()
                val parTime = endTime - startTime

                Platform.runLater({
                    seqTimeLabel.text = "$seqTime мс"
                    parTimeLabel.text = "$parTime мс"
                    graphSizeChoiceBox.isDisable = false
                    startBtn.isDisable = false
                })
            })
            calcThread.start()
        }
    }

}