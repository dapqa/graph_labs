package graph.labs.common.controller

/**
 * @author Nechaev Alexander
 */

interface IRegenerateListener {

    fun onRegenerate(id: String)

}