package graph.labs.common.controller

import graph.labs.common.Graph

/**
 * @author Nechaev Alexander
 */
abstract class BaseLabController : IRegenerateListener {

    protected fun initAdjacencyMatrix(controller: AdjacencyMatrixController, regenerateId: String) {
        controller.regenerateId = regenerateId
        controller.regenerateListener = this
        controller.nodeCountField.text = "${getDefaultNodeCountToGenerate()}"
        controller.regenerate(null)
    }

    protected data class GUIGraphDescriptor(val graph: Graph, val matrixController: AdjacencyMatrixController, val graphController: GraphPaneController)

    protected abstract fun getGUIGraphDescriptorById(id: String): GUIGraphDescriptor

    override fun onRegenerate(id: String) {
        val ggd = getGUIGraphDescriptorById(id)

        val matrixController = ggd.matrixController
        val graphController = ggd.graphController
        val graph = ggd.graph

        if (!matrixController.generatingDisabled) {
            // В противном случае должно заранее сгенериться
            try {
                val nodeCount = matrixController.getNodeCount()
                if (nodeCount !in getMinNodeCountToGenerate()..getMaxNodeCountToGenerate()) {
                    throw IllegalArgumentException("Количество вершин должно быть в промежутке от ${getMinNodeCountToGenerate()} до ${getMaxNodeCountToGenerate()}")
                }

                graph.regenerate(nodeCount)
            } catch (e: IllegalArgumentException) {
                showError(if (e.message?.startsWith("For input string") == false) e.message else "Количество вершин задается целым числом")
            }
        }

        graphController.drawGraph(graph)
        matrixController.refreshMatrix(graph.adjacencyMatrix, graph.nodeNames)
    }

    protected abstract fun getDefaultNodeCountToGenerate(): Int
    protected abstract fun getMinNodeCountToGenerate(): Int
    protected abstract fun getMaxNodeCountToGenerate(): Int

}