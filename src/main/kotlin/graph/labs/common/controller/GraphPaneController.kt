package graph.labs.common.controller

import graph.labs.common.Graph
import javafx.embed.swing.SwingNode
import javafx.fxml.FXML
import javafx.scene.layout.StackPane
import org.graphstream.graph.Edge
import org.graphstream.graph.Node
import org.graphstream.graph.implementations.SingleGraph
import org.graphstream.ui.swingViewer.Viewer

import org.graphstream.graph.Graph as GraphGS

/**
 * @author Nechaev Alexander
 */

class GraphPaneController {

    private val swingNode = SwingNode()
    lateinit var gsGraph: GraphGS

    @FXML lateinit var swingNodeContainer: StackPane

    @FXML
    fun initialize() {
        swingNodeContainer.children.add(swingNode)
    }

    fun drawGraph(graph: Graph) {
        gsGraph = SingleGraph("G")
        val adjMatrix = graph.adjacencyMatrix
        val nodeCount = graph.nodeCount

        for (i in 0..nodeCount - 1) {
            val node = gsGraph.addNode<Node>("$i")
            node.addAttribute("ui.size", 20.0)
            node.addAttribute("ui.label", graph.nodeNames.get(i))
        }

        // Ориентированность?
        val size = adjMatrix.size
        for (i in 0..size - 1) {
            for (j in 0..size - 1) {
                if (adjMatrix[i][j] != 0.toByte()) {
                    gsGraph.addEdge<Edge>("$i-$j", "$i", "$j", true)
                }
            }
        }

        gsGraph.addAttribute("ui.stylesheet", "node {" +
                "fill-color: #44f;" +
                "size-mode: dyn-size;" +
                "text-color: #fff;" +
                "text-style: bold;" +
                "text-size: 12px;" +
        "}")

        val viewer = Viewer(gsGraph, Viewer.ThreadingModel.GRAPH_IN_SWING_THREAD)
        val view = viewer.addDefaultView(false)
        viewer.enableAutoLayout()
        viewer.closeFramePolicy = Viewer.CloseFramePolicy.EXIT
        swingNode.content = view

        // Это нужно для того, чтобы swingNode не выходила за пределы панели
        swingNode.resize(440.0, 570.0) // TODO Можно ведь как-то не захардкодить, а посчитать?
    }

}