package graph.labs.common.controller

import javafx.scene.control.Alert
import javafx.scene.control.ButtonType

/**
 * @author Nechaev Alexander
 */

fun showError(message: String?) {
    val alert = Alert(Alert.AlertType.ERROR, message, ButtonType.OK)
    alert.headerText = ""
    alert.title = "Ошибка"
    alert.showAndWait()
}