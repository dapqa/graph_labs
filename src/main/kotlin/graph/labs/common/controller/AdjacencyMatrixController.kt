package graph.labs.common.controller

import com.sun.javafx.collections.ObservableListWrapper
import javafx.beans.property.ReadOnlyStringWrapper
import javafx.event.Event
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.util.Callback

/**
 * @author Nechaev Alexander
 */

class AdjacencyMatrixController {

    lateinit var regenerateListener: IRegenerateListener
    var regenerateId = "DEFAULT"
    var generatingDisabled = false

    @FXML lateinit var label1: Label
    @FXML lateinit var label2: Label
    @FXML lateinit var nodeCountField: TextField
    @FXML lateinit var regenerateBtn: Button
    @FXML lateinit var adjMatrixTable: TableView<ByteArray>

    @FXML
    fun initialize() {
        adjMatrixTable.isVisible = false
        adjMatrixTable.selectionModel = null
        adjMatrixTable.columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
    }

    fun regenerate(event: Event?) {
        regenerateListener.onRegenerate(regenerateId)
    }

    fun getNodeCount(): Int = Integer.parseInt(nodeCountField.text)

    fun refreshMatrix(matrix: Array<ByteArray>) {
        refreshMatrix(matrix, null)
    }

    fun refreshMatrix(matrix: Array<ByteArray>, nodeNames: List<String>?) {
        adjMatrixTable.isVisible = true
        adjMatrixTable.columns.clear()

        val nodeCount = matrix.size

        val rowHeaderColumn = TableColumn<ByteArray, String>("")
        rowHeaderColumn.cellValueFactory = Callback { cd ->
            return@Callback ReadOnlyStringWrapper(
                    "${if (nodeNames == null) matrix.indexOf(cd.value) + 1 else nodeNames.get(matrix.indexOf(cd.value))}"
            )
        }
        rowHeaderColumn.styleClass.add("column-header")
        adjMatrixTable.columns.add(rowHeaderColumn)

        for (i in 0..nodeCount - 1) {
            val column = TableColumn<ByteArray, String>("${if (nodeNames == null) i + 1 else nodeNames.get(i)}")
            column.cellValueFactory = Callback { cd -> ReadOnlyStringWrapper("${cd.value[i]}")  }
            column.isSortable = false
            adjMatrixTable.columns.add(column)
        }

        adjMatrixTable.items = ObservableListWrapper<ByteArray>(matrix.asList())
    }

    fun disableGenerating() {
        regenerateBtn.isVisible = false
        nodeCountField.isVisible = false
        label1.isVisible = false
        label2.isVisible = false

        generatingDisabled = true
    }

}