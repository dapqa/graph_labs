package graph.labs.common

/**
 * @author Nechaev Alexander
 */

val CORE_COUNT = Runtime.getRuntime().availableProcessors()

// Метод для расчета индексов, с которыми будут работать потоки
// при распараллеливании циклов
fun calcParts(from: Int, to: Int): Array<IntArray> {
    val res = Array(CORE_COUNT, { IntArray(2, { 0 }) })

    val length = to - from + 1
    val oneCoreLength = length / CORE_COUNT
    for (i in 0..CORE_COUNT - 1) {
        res[i][0] = i * oneCoreLength + from
        res[i][1] = (i + 1) * oneCoreLength + from - 1
    }
    res[CORE_COUNT - 1][1] = to

    return res
}