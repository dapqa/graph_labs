package graph.labs.lab3.controller

import graph.labs.common.Graph
import graph.labs.common.controller.AdjacencyMatrixController
import graph.labs.common.controller.BaseLabController
import graph.labs.common.controller.GraphPaneController
import graph.labs.lab3.makeOriginatedSubgraphWithEvenNodes
import graph.labs.lab3.makeSpanningSubgraphWithOddNodes
import javafx.fxml.FXML

/**
 * @author Nechaev Alexander
 */
class Lab3Controller : BaseLabController() {

    @FXML lateinit var graph1PaneController: GraphPaneController
    @FXML lateinit var graph2PaneController: GraphPaneController
    @FXML lateinit var graph3PaneController: GraphPaneController
    @FXML lateinit var adjacencyMatrix1Controller: AdjacencyMatrixController
    @FXML lateinit var adjacencyMatrix2Controller: AdjacencyMatrixController
    @FXML lateinit var adjacencyMatrix3Controller: AdjacencyMatrixController

    private val MATRIX1_ID = "M1"

    var graph1: Graph = Graph()
    var graph2: Graph = Graph()
    var graph3: Graph = Graph()

    @FXML
    fun initialize() {
        initAdjacencyMatrix(adjacencyMatrix1Controller, MATRIX1_ID)

        adjacencyMatrix2Controller.disableGenerating()
        adjacencyMatrix3Controller.disableGenerating()
    }

    override fun onRegenerate(id: String) {
        super.onRegenerate(id)

        graph2 = makeOriginatedSubgraphWithEvenNodes(graph1)
        adjacencyMatrix2Controller.refreshMatrix(graph2.adjacencyMatrix, graph2.nodeNames)
        graph2PaneController.drawGraph(graph2)

        graph3 = makeSpanningSubgraphWithOddNodes(graph1)
        adjacencyMatrix3Controller.refreshMatrix(graph3.adjacencyMatrix, graph3.nodeNames)
        graph3PaneController.drawGraph(graph3)
    }

    override fun getGUIGraphDescriptorById(id: String): GUIGraphDescriptor {
        // Кнопка генерировать есть только на первом контроллере
        return GUIGraphDescriptor(graph1, adjacencyMatrix1Controller, graph1PaneController)
    }

    override fun getDefaultNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMinNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMaxNodeCountToGenerate(): Int {
        return 10
    }

}