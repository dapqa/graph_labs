package graph.labs.lab3

import graph.labs.common.BaseLab
import graph.labs.common.Graph
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */
class Lab3 : BaseLab<Lab3>() {

    override fun getLayout(): String {
        return "Lab3.fxml"
    }

    override fun getLabClass(): KClass<Lab3> {
        return Lab3::class
    }

    override fun getName(): String {
        return "Лабораторная работа #3 по ТиМДВ"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Lab3::class.java)
        }
    }

}

fun makeSpanningSubgraphWithOddNodes(graph: Graph): Graph {
    val adjMatrix = graph.adjacencyMatrix
    val nodeCount = graph.nodeCount
    val resAdjMatrix = Array(nodeCount, { ByteArray(nodeCount, { 0 })})

    for (i in 0..nodeCount - 1) {
        for (j in 0..nodeCount - 1) {
            if ((i + j) % 2 == 0) continue
            resAdjMatrix[i][j] = adjMatrix[i][j]
        }
    }

    // Имена вершин не надо менять, они и до этого не менялись
    val res = Graph()
    res.adjacencyMatrix = resAdjMatrix
    return res
}

fun makeOriginatedSubgraphWithEvenNodes(graph: Graph): Graph {
    val adjMatrix = graph.adjacencyMatrix
    val nodeCount = graph.nodeCount
    val resNodeCount = nodeCount / 2
    val resAdjMatrix = Array(resNodeCount, { ByteArray(resNodeCount, { 0 })})

    // Нумерация вершин с 1, массива - с 0, поэтому так
    for (i in 1..nodeCount - 1 step 2) {
        for (j in 1..nodeCount - 1 step 2) {
            resAdjMatrix[i / 2][j / 2] = adjMatrix[i][j]
        }
    }

    val res = Graph()
    res.adjacencyMatrix = resAdjMatrix
    for (i in 0..resNodeCount - 1) {
        res.nodeNames.set(i, "${(i + 1) * 2}")
    }
    return res
}