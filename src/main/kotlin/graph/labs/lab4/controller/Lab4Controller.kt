package graph.labs.lab4.controller

import graph.labs.common.Graph
import graph.labs.common.controller.AdjacencyMatrixController
import graph.labs.common.controller.BaseLabController
import graph.labs.common.controller.GraphPaneController
import graph.labs.common.controller.ParallelCheckerPaneController
import graph.labs.common.makeTransposedGraph
import graph.labs.lab4.findDirectTransitiveClosureVNames
import graph.labs.lab4.findStrongComponents
import graph.labs.lab4.findStrongComponentsParallel
import graph.labs.lab4.findStrongComponentsVNames
import javafx.fxml.FXML
import javafx.scene.control.Label
import java.util.function.Consumer

/**
 * @author Nechaev Alexander
 */
class Lab4Controller : BaseLabController() {

    @FXML lateinit var graphPaneController: GraphPaneController
    @FXML lateinit var adjacencyMatrixController: AdjacencyMatrixController
    @FXML lateinit var parallelCheckerPaneController: ParallelCheckerPaneController
    @FXML lateinit var dirTransClosureLabel: Label
    @FXML lateinit var invTransClosureLabel: Label
    @FXML lateinit var strongComponentsLabel: Label

    private val MATRIX_ID = "M1"
    private val NODE_NUMBER = 4 // Вариант 5

    var graph: Graph = Graph()

    @FXML
    fun initialize() {
        initAdjacencyMatrix(adjacencyMatrixController, MATRIX_ID)

        parallelCheckerPaneController.seqConsumer = Consumer { g -> findStrongComponents(g) }
        parallelCheckerPaneController.parConsumer = Consumer { g -> findStrongComponentsParallel(g) }
    }

    override fun onRegenerate(id: String) {
        super.onRegenerate(id)

        dirTransClosureLabel.text = findDirectTransitiveClosureVNames(graph, NODE_NUMBER).toString()
        invTransClosureLabel.text = findDirectTransitiveClosureVNames(makeTransposedGraph(graph), NODE_NUMBER).toString()
        strongComponentsLabel.text = findStrongComponentsVNames(graph).toString()
    }

    override fun getGUIGraphDescriptorById(id: String): GUIGraphDescriptor {
        return GUIGraphDescriptor(graph, adjacencyMatrixController, graphPaneController)
    }

    override fun getDefaultNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMinNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMaxNodeCountToGenerate(): Int {
        return 10
    }

}