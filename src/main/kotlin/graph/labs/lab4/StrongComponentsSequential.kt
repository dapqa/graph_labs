package graph.labs.lab4

import graph.labs.common.Graph
import graph.labs.common.findDirectTransitiveClosure
import graph.labs.common.nodeNumbersToNames2
import graph.labs.common.transpose
import java.util.*

/**
 * @author Nechaev Alexander
 * Поиск сильных компонент матричным методом (последовательный)
 */

// Расчет матрицы достижимости
fun makeAttainabilityMatrix(graph: Graph): Array<ByteArray> {
    val nodeCount = graph.nodeCount
    val res = Array(nodeCount, { ByteArray(nodeCount, { 0 })})

    for (i in 0..nodeCount - 1) {
        val dirTransitiveClosure = findDirectTransitiveClosure(graph, i);
        for (j in 0..dirTransitiveClosure.size - 1) {
            res[i][dirTransitiveClosure.get(j)] = 1
        }
    }

    return res
}

// Поиск сильных компонент матричным методом
fun findStrongComponents(graph: Graph): List<List<Int>> {
    val res = ArrayList<List<Int>>()
    val nodeCount = graph.nodeCount

    val attainabilityMatrix = makeAttainabilityMatrix(graph)
    val invAttainabilityMatrix = transpose(attainabilityMatrix)
    for (i in 0..nodeCount - 1) {
        for (j in 0..nodeCount - 1) {
            attainabilityMatrix[i][j] = (attainabilityMatrix[i][j].toInt() and invAttainabilityMatrix[i][j].toInt()).toByte()
        }
    }

    val deleted = Array(nodeCount, { false })
    var visitedCount = 0
    var cur = 0
    var curComponent: List<Int>
    while (visitedCount < nodeCount) {
        curComponent = ArrayList<Int>()
        for (i in 0..nodeCount - 1) {
            if (!deleted[i] && attainabilityMatrix[cur][i].toInt() == 1) {
                deleted[i] = true
                if (i != cur) {
                    visitedCount++
                    curComponent.add(i)
                }
            }
        }
        visitedCount++
        curComponent.add(cur)
        res.add(curComponent)

        for (i in cur + 1..nodeCount - 1) {
            if (!deleted[i]) {
                cur = i
                break
            }
        }
    }

    // Отсеиваются подграфы, состоящие из одной вершины и являющиеся частью других компонент
    // сильной связности
    val fixedRes = ArrayList<List<Int>>()
    outer@ for (i in 0..res.size - 1) {
        if (res.get(i).size == 1) {
            val nodeNumber = res.get(i).get(0)
            if (graph.adjacencyMatrix[nodeNumber][nodeNumber].toInt() == 0) {
                for (j in 0..res.size - 1) {
                    if (i == j) continue

                    if (res.get(j).contains(nodeNumber)) {
                        continue@outer
                    }
                }
            }
        }

        fixedRes.add(res.get(i))
    }

    return fixedRes
}

fun findStrongComponentsVNames(graph: Graph): List<List<String>> {
    return nodeNumbersToNames2(graph, findStrongComponents(graph))
}
