package graph.labs.lab4

import graph.labs.common.*
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * @author Nechaev Alexander
 * Поиск сильных компонент матричным методом (параллельный)
 */

// Расчет матрицы достижимости в несколько потоков
fun makeAttainabilityMatrixParallel(graph: Graph): Array<ByteArray> {
    val nodeCount = graph.nodeCount
    val res = Array(nodeCount, { ByteArray(nodeCount, { 0 })})

    val executorService = Executors.newFixedThreadPool(CORE_COUNT)
    val parts = calcParts(0, nodeCount - 1)
    for (k in 0..CORE_COUNT - 1) {
        executorService.execute {
            for (i in parts[k][0]..parts[k][1]) {
                val dirTransitiveClosure = findDirectTransitiveClosure(graph, i);
                for (j in 0..dirTransitiveClosure.size - 1) {
                    res[i][dirTransitiveClosure.get(j)] = 1
                }
            }
        }
    }
    executorService.shutdown()
    executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)

    return res
}

// Поиск сильных компонент в несколько потоков
fun findStrongComponentsParallel(graph: Graph): List<List<Int>> {
    val res = ArrayList<List<Int>>()
    val nodeCount = graph.nodeCount

    val attainabilityMatrix = makeAttainabilityMatrixParallel(graph)
    val invAttainabilityMatrix = transpose(attainabilityMatrix)
    for (i in 0..nodeCount - 1) {
        for (j in 0..nodeCount - 1) {
            attainabilityMatrix[i][j] = (attainabilityMatrix[i][j].toInt() and invAttainabilityMatrix[i][j].toInt()).toByte()
        }
    }

    val deleted = Array(nodeCount, { false })
    var visitedCount = 0
    var cur = 0
    var curComponent: List<Int>
    while (visitedCount < nodeCount) {
        curComponent = ArrayList<Int>()
        for (i in 0..nodeCount - 1) {
            if (!deleted[i] && attainabilityMatrix[cur][i].toInt() == 1) {
                deleted[i] = true
                if (i != cur) {
                    visitedCount++
                    curComponent.add(i)
                }
            }
        }
        visitedCount++
        curComponent.add(cur)
        res.add(curComponent)

        for (i in cur + 1..nodeCount - 1) {
            if (!deleted[i]) {
                cur = i
                break
            }
        }
    }

    // В несколько потоков отсеиваются подграфы, состоящие из одной вершины и являющиеся частью других компонент
    // сильной связности
    val fixedRes = ArrayList<List<Int>>()
    val executorService = Executors.newFixedThreadPool(CORE_COUNT)
    val parts = calcParts(0, res.size - 1)
    for (k in 0..CORE_COUNT - 1) {
        executorService.execute {
            outer@ for (i in parts[k][0]..parts[k][1]) {
                if (res.get(i).size == 1) {
                    val nodeNumber = res.get(i).get(0)
                    if (graph.adjacencyMatrix[nodeNumber][nodeNumber].toInt() == 0) {
                        for (j in 0..res.size - 1) {
                            if (i == j) continue

                            if (res.get(j).contains(nodeNumber)) {
                                continue@outer
                            }
                        }
                    }
                }

                fixedRes.add(res.get(i))
            }
        }
    }
    executorService.shutdown()
    executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)

    return fixedRes
}
