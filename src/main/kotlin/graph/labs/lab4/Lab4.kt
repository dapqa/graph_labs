package graph.labs.lab4

import graph.labs.common.BaseLab
import graph.labs.common.Graph
import graph.labs.common.findDirectTransitiveClosure
import graph.labs.common.nodeNumbersToNames
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */
class Lab4 : BaseLab<Lab4>() {

    override fun getLayout(): String {
        return "Lab4.fxml"
    }

    override fun getLabClass(): KClass<Lab4> {
        return Lab4::class
    }

    override fun getName(): String {
        return "Лабораторная работа #4 по ТиМДВ"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Lab4::class.java)
        }
    }

}

fun findDirectTransitiveClosureVNames(graph: Graph, nodeNumber: Int): List<String> {
    return nodeNumbersToNames(graph, findDirectTransitiveClosure(graph, nodeNumber));
}