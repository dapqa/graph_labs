package graph.labs.lab2.controller

import com.sun.javafx.collections.ObservableListWrapper
import graph.labs.common.Graph
import graph.labs.common.IncidenceMatrix
import graph.labs.common.controller.AdjacencyMatrixController
import graph.labs.common.controller.BaseLabController
import graph.labs.common.controller.GraphPaneController
import graph.labs.lab2.hasTwoWayEdges
import javafx.beans.property.ReadOnlyStringWrapper
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.util.Callback

/**
 * @author Nechaev Alexander
 */
class Lab2Controller : BaseLabController() {

    @FXML lateinit var graphPaneController: GraphPaneController
    @FXML lateinit var adjacencyMatrixController: AdjacencyMatrixController
    @FXML lateinit var incMatrixTable: TableView<ByteArray>
    @FXML lateinit var twoWayEdgesLabel: Label

    private val MATRIX_ID = "M1"

    var graph: Graph = Graph()

    @FXML
    fun initialize() {
        initAdjacencyMatrix(adjacencyMatrixController, MATRIX_ID)
    }

    override fun onRegenerate(id: String) {
        super.onRegenerate(id)

        val incMatrix = refreshIncMatrixTable()
        twoWayEdgesLabel.text = if (hasTwoWayEdges(incMatrix)) "ЕСТЬ" else "ОТСТУТСТВУЮТ"
    }

    fun refreshIncMatrixTable(): IncidenceMatrix {
        incMatrixTable.columns.clear()
        val incMatrix = graph.makeIncidenceMatrix()

        val edgeCount = incMatrix.matrix[0].size

        val rowHeaderColumn = TableColumn<ByteArray, String>("")
        rowHeaderColumn.cellValueFactory = Callback { cd ->
            return@Callback ReadOnlyStringWrapper(
                    incMatrix.nodeNames.get(incMatrix.matrix.indexOf(cd.value))
            )
        }
        rowHeaderColumn.styleClass.add("column-header")
        incMatrixTable.columns.add(rowHeaderColumn)

        for (i in 0..edgeCount - 1) {
            val column = TableColumn<ByteArray, String>(incMatrix.edgeNames.get(i))
            column.cellValueFactory = Callback { cd -> ReadOnlyStringWrapper("${if (cd.value[i].toInt() != 2) cd.value[i] else "0*" }")  }
            column.isSortable = false
            incMatrixTable.columns.add(column)
        }

        incMatrixTable.items = ObservableListWrapper<ByteArray>(incMatrix.matrix.asList())

        return incMatrix
    }

    override fun getGUIGraphDescriptorById(id: String): GUIGraphDescriptor {
        return GUIGraphDescriptor(graph, adjacencyMatrixController, graphPaneController)
    }

    override fun getDefaultNodeCountToGenerate(): Int {
        return 5
    }

    override fun getMinNodeCountToGenerate(): Int {
        return 4
    }

    override fun getMaxNodeCountToGenerate(): Int {
        return 8
    }

}