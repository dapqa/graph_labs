package graph.labs.lab2

import graph.labs.common.BaseLab
import graph.labs.common.IncidenceMatrix
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */
class Lab2 : BaseLab<Lab2>() {

    override fun getLayout(): String {
        return "Lab2.fxml"
    }

    override fun getLabClass(): KClass<Lab2> {
        return Lab2::class
    }

    override fun getName(): String {
        return "Лабораторная работа #2 по ТиМДВ"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Lab2::class.java)
        }
    }

}

fun hasTwoWayEdges(incMatrix: IncidenceMatrix): Boolean {
    val nodeCount = incMatrix.matrix.size
    val edgeCount = incMatrix.matrix[0].size
    var got1: Boolean
    var got2: Boolean

    for (i in 0..edgeCount - 1) {
        for (j in i + 1..edgeCount - 1) {
            got1 = false
            got2 = false
            for (k in 0..nodeCount - 1) {
                if (incMatrix.matrix[k][i].toInt() == 1 && incMatrix.matrix[k][j].toInt() == -1) {
                    got1 = true
                } else if (incMatrix.matrix[k][i].toInt() == -1 && incMatrix.matrix[k][j].toInt() == 1) {
                    got2 = true
                }
            }
            if (got1 && got2) return true
        }
    }
    return false
}