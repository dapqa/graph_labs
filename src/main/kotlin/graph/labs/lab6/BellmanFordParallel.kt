package graph.labs.lab6

import graph.labs.common.CORE_COUNT
import graph.labs.common.Graph
import graph.labs.common.calcParts
import graph.labs.common.makeEdgeList
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * @author Nechaev Alexander
 */

// Параллельная реализация алгоритма Беллмана-Форда
fun makeSsspBaseParallel(graph: Graph, from: Int): Array<List<Int>> {
    val nodeCount = graph.nodeCount
    val edges = makeEdgeList(graph)

    val dist = IntArray(nodeCount, { INFINITY })
    val prev = IntArray(nodeCount, { -1 }) // Массив для восстановления пути

    dist[from] = 0
    var any = true
    val parts = calcParts(0, edges.size - 1)
    var executorService: ExecutorService
    while (any) {
        any = false

        executorService = Executors.newFixedThreadPool(CORE_COUNT)
        for (k in 0..CORE_COUNT - 1) {
            executorService.execute {
                for (i in parts[k][0]..parts[k][1]) {
                    val edge = edges.get(i)
                    if (dist[edge.to] > dist[edge.from] + edge.weight) {
                        dist[edge.to] = dist[edge.from] + edge.weight
                        prev[edge.to] = edge.from
                        any = true
                    }
                }
            }
        }
        executorService.shutdown()
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)
    }

    // Восстановление путей
    return Array(nodeCount, {
        val path = ArrayList<Int>()
        var cur = it

        while(prev[cur] != -1) {
            path.add(cur)
            cur = prev[cur]
        }

        if (it != from) {
            path.add(from)
        }

        path.reversed()
    })
}