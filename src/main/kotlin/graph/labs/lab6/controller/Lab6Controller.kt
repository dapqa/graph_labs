package graph.labs.lab6.controller

import graph.labs.common.Graph
import graph.labs.common.controller.AdjacencyMatrixController
import graph.labs.common.controller.BaseLabController
import graph.labs.common.controller.GraphPaneController
import graph.labs.common.controller.ParallelCheckerPaneController
import graph.labs.lab6.*
import javafx.fxml.FXML
import javafx.scene.control.Label
import java.util.function.Consumer

/**
 * @author Nechaev Alexander
 */
class Lab6Controller : BaseLabController() {

    @FXML lateinit var graphPaneController: GraphPaneController
    @FXML lateinit var adjacencyMatrixController: AdjacencyMatrixController
    @FXML lateinit var ssspBaseLabel: Label
    @FXML lateinit var parallelCheckerPaneController: ParallelCheckerPaneController

    private val MATRIX_ID = "M1"

    var graph: Graph = Graph(weighted = true)

    @FXML
    fun initialize() {
        graph.adjacencyMatrix = createC()
        adjacencyMatrixController.disableGenerating()
        initAdjacencyMatrix(adjacencyMatrixController, MATRIX_ID)

        val ssspBaseVNames = makeSsspBaseVNames(graph, FROM)
        val resBuilder = StringBuilder()
        ssspBaseVNames.forEachIndexed { i, path ->
            resBuilder.append("${graph.nodeNames.get(i)}: ${path.toString()}\n")
        }
        ssspBaseLabel.text = resBuilder.toString()

        parallelCheckerPaneController.useWeighted = true
        parallelCheckerPaneController.maxNodeCount = 6400
        parallelCheckerPaneController.seqConsumer = Consumer { g -> makeSsspBase(g, FROM) }
        parallelCheckerPaneController.parConsumer = Consumer { g -> makeSsspBaseParallel(g, FROM) }
    }

    override fun onRegenerate(id: String) {
        super.onRegenerate(id)
    }

    override fun getGUIGraphDescriptorById(id: String): GUIGraphDescriptor {
        return GUIGraphDescriptor(graph, adjacencyMatrixController, graphPaneController)
    }

    // Здесь они не нужны
    override fun getDefaultNodeCountToGenerate(): Int = 0
    override fun getMinNodeCountToGenerate(): Int = 0
    override fun getMaxNodeCountToGenerate(): Int = 0

}