package graph.labs.lab6

import graph.labs.common.BaseLab
import kotlin.reflect.KClass

/**
 * @author Nechaev Alexander
 */
class Lab6 : BaseLab<Lab6>() {

    override fun getLayout(): String {
        return "Lab6.fxml"
    }

    override fun getLabClass(): KClass<Lab6> {
        return Lab6::class
    }

    override fun getName(): String {
        return "Лабораторная работа #6 по ТиМДВ"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Lab6::class.java)
        }
    }

}

fun createC(): Array<ByteArray> {
    return arrayOf(
            byteArrayOf(0, 2, 5, 3, 0, 4, 0, 0),
            byteArrayOf(4, 0, 0, 8, 0, 15, 0, 0),
            byteArrayOf(9, 1, 0, 2, 5, 0, 13, 0),
            byteArrayOf(0, 8, 2, 0, 16, 0, 3, 0),
            byteArrayOf(0, 0, 5, 16, 0, 0, 0, 4),
            byteArrayOf(4, 15, 0, 0, 0, 6, 7, 0),
            byteArrayOf(0, 0, 13, 0, 0, 7, 0, 3),
            byteArrayOf(0, 0, 0, 0, 4, 0, 0, 0)
    )
}

val FROM = 4
val INFINITY = Integer.MAX_VALUE - 1000000