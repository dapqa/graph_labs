package graph.labs.lab6

import graph.labs.common.Graph
import graph.labs.common.makeEdgeList
import java.util.*

/**
 * @author Nechaev Alexander
 */

// Последовательная реализация алгоритма Беллмана-Форда
fun makeSsspBase(graph: Graph, from: Int): Array<List<Int>> {
    val nodeCount = graph.nodeCount
    val edges = makeEdgeList(graph)

    val dist = IntArray(nodeCount, { INFINITY })
    val prev = IntArray(nodeCount, { -1 }) // Массив для восстановления пути

    dist[from] = 0
    var any = true
    while (any) {
        any = false
        for (i in 0..edges.size - 1) {
            val edge = edges.get(i)
            if (dist[edge.to] > dist[edge.from] + edge.weight) {
                dist[edge.to] = dist[edge.from] + edge.weight
                prev[edge.to] = edge.from
                any = true
            }
        }
    }

    // Восстановление путей
    return Array(nodeCount, {
        val path = ArrayList<Int>()
        var cur = it

        while(prev[cur] != -1) {
            path.add(cur)
            cur = prev[cur]
        }

        if (it != from) {
            path.add(from)
        }

        path.reversed()
    })
}

fun makeSsspBaseVNames(graph: Graph, from: Int): Array<List<String>> {
    val ssspBase = makeSsspBase(graph, from)
    return Array(graph.nodeCount, {
        ssspBase[it].map { n -> graph.nodeNames.get(n) }
    })
}